import React from 'react';
import Image from 'next/image';

export default function Card({ photos, author, url }) {
  return (
    <div className="flex-none">
      <div className="w-40 bg-white rounded-[10px] h-full border">
        <div className="rounded-[10px]">
          <div className="flex items-baseline">
            <Image
              alt={author}
              src={photos}
              width={500}
              height={400}
              className="object-cover rounded-t-[10px]"
            />
          </div>
          <div className="py-1 px-2 border w-full h-full">
            <h1 className="text-lg font-bold tracking-tight text-gray-900 ">{author}</h1>
            <a target="__blank" href={url}>
              <span className="text-xs text-center">{url}</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
