import axios from 'axios';
import { useEffect, useState } from 'react';
import { ChevronRightIcon, ChevronLeftIcon } from '@heroicons/react/solid';
import Card from './card';

export default function Carousel() {
  const [photos, setPhotos] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getPhotos();
  }, []);

  async function getPhotos() {
    const response = await axios.get(`https://picsum.photos/v2/list`, {
      params: {
        page: 1,
        limit: 40,
      },
    });
    setPhotos(response?.data);
    setLoading(false);
  }

  const scrollButton = (shift) => {
    const slider = document.getElementById('slider');
    slider.scrollLeft = slider.scrollLeft += shift;
  };

  return (
    <>
      <div className="w-full">
        <div className="flex justify-between items-center">
          <h1 className="font-bold">Photos</h1>
          <div className="flex">
            <ChevronLeftIcon onClick={() => scrollButton(-500)} className="w-7 text-black border" />
            <ChevronRightIcon onClick={() => scrollButton(500)} className="w-7 text-black border" />
          </div>
        </div>
      </div>

      <div id="slider" className="flex space-x-2 overflow-scroll scroll scroll-smooth">
        {loading ? (
          <div>Loading</div>
        ) : (
          <>
            {photos.map((photo, index) => {
              return (
                <Card
                  key={index}
                  author={photo.author}
                  photos={photo.download_url}
                  url={photo.url}
                />
              );
            })}
          </>
        )}
      </div>
    </>
  );
}
