import { Dialog, Transition } from '@headlessui/react';
import Image from 'next/image';
import React, { Fragment, useState } from 'react';

export default function PopUp() {
  const [isOpen, setIsOpen] = useState(false);

  const handleOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <>
      <button className="px-3 py-1" onClick={handleOpen}>
        Open pop up
      </button>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" open={isOpen} onClose={handleClose}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-200/70" />
          </Transition.Child>
          <div className="fixed inset-0">
            <div className="flex h-screen items-center justify-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-50"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="max-w-xs rounded-xl bg-white z-10 px-10 py-4 space-y-4 shadow-md">
                  <Dialog.Title as="div" className="text-gray-900 flex items-center flex-col">
                    <Image src="/ajukan-1.png" width={124} height={124} layout="fixed" />

                    <p className="font-bold text-center">
                      Apakah anda yakin ingin mengajukan mentoring?
                    </p>
                    <p className=" text-xs text-center mt-2">
                      Mentor akan mengkonfirmasi terkait waktu pengajuan Anda
                    </p>

                    <button className="rounded-md py-2 px-3 font-semibold text-white text-sm bg-blue-500 h-9 mt-4 mb-2 w-64 hover:bg-blue-600">
                      Ajukan Permintaan Mentoring
                    </button>
                    <button
                      onClick={handleClose}
                      className="text-sm rounded-md border py-[7px] w-64 hover:bg-gray-100"
                    >
                      Batalkan
                    </button>
                  </Dialog.Title>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
