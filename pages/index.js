import { Fragment } from 'react';
import Carousel from '../components/carousel';
import Layout from '../layout';
import PopUp from '../components/popup';

export default function Home() {
  return (
    <Fragment>
      {/* <Layout /> */}
      <div className="min-h-screen flex items-center justify-center">
        <div className="container mx-auto">
          <Carousel />
          {/* <PopUp /> */}
        </div>
      </div>
    </Fragment>
  );
}
